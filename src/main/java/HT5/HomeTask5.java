package HT5;
import java.util.Scanner;

public class HomeTask5 {
    public static void main(String[] args) {
       // System.out.println(isPrime(0));
       // System.out.println(isPrime(8));
       // System.out.println(isPrime(7));
       // System.out.println(getGrade(5));
      //  System.out.println(getGrade(1));
       // System.out.println(getGrade(-3));
        //System.out.println(calculateSumUpToN(1));
        //System.out.println(calculateSumUpToN(3));
        //System.out.println(calculateSumUpToN(0));
        //Scanner scanner = new Scanner(System.in);
        //System.out.println("Введіть число: ");
        //int number = scanner.nextInt();
        //System.out.println("Чи є число квадратом цілого числа? " + isPerfectSquare(number));
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введіть ваш вік: ");
        int age = scanner.nextInt();
        System.out.println(checkAge(age));

    }
    //
//Напишіть програму, яка приймає число N та перевіряє, чи є воно простим числом.
//
//isPrime(0) → false
//isPrime(8) → false
//isPrime(7) → true
    public  static boolean  isPrime(int n) {
        if (n < 2) {
            return false;
        }
        for (int i =2; i<=Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }

    return true;

    }
//Напишіть програму, яка приймає числове значення від 1 до 5 та виводить відповідну оцінку студента.
//
//getGrade(5) → "Відмінно"
//getGrade(1) → "Погано"
//getGrade(-3) → "Неправильна оцінка"

    public static String getGrade(int grade)
    {
        switch (grade) {
            case 5:
                return  "Відмінно";
            case 4:
                return  "Добре";
            case 3:
                return  "Задовільно";
            case 2:
                return  "Погано";
            case 1:
                return  "Дуже погано";
            default:
                return "Неправильна оцінка";
        }
    }
//Напишіть програму, яка приймає число N та виводить суму всіх чисел від 1 до N.
//calculateSumUpToN(1) → “1”
//calculateSumUpToN(3) → “1 + 2 + 3”
//calculateSumUpToN(0) → “Хибні вхідні параметри”


    public static String calculateSumUpToN(int N) {
        if (N <= 0) {
            return  "Хибні вхідні параметри";
        }
        StringBuilder result = new StringBuilder();
        int sum = 0;
        for (int i = 1; i <= N; i++) {
            sum += i;
            result.append(i);
            if (i < N) {
                result.append(" + ");
            }
        }
        result.append(" = ").append(sum);
        return  result.toString();
    }
//Напишіть програму, яка приймає ціле число від користувача та перевіряє, чи є воно квадратом цілого числа.
//isPerfectSquare(25) → true
//isPerfectSquare(20) → false
//isPerfectSquare(9) → true
   public static boolean isPerfectSquare(int number) {
        if (number < 0) {
            return false;
        }
        int sqrt = (int) Math.sqrt(number);
        return  sqrt * sqrt == number;
   }
//Напишіть програму, яка приймає вік користувача та виводить повідомлення, чи він є дорослим (вік 18 і більше) чи ні.
//checkAge(18) → "Ви доросла особа"
//checkAge(0) → "Ви не є дорослою особою"
//checkAge(99) → "Ви доросла особа"
   public static String checkAge(int age) {
        if (age >=18) {
            return "Ви доросла особа";
        }
        else {
            return "Ви не є дорослою особою";
        }
   }

}





